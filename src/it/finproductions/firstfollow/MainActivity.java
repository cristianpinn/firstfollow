package it.finproductions.firstfollow;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	TextView tx;
	private int numeroProduzioni;
	Button vai, calcola, reset;
	TableLayout tableLayout, leftfirstfollowtable;
	boolean errore = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tableLayout = (TableLayout)findViewById(R.id.tabellaContenuti);
		leftfirstfollowtable = (TableLayout)findViewById(R.id.tabellaContenutiLast);
		tx = (TextView)findViewById(R.id.txt_numero_produzioni);
		vai = (Button)findViewById(R.id.btn_vai_left_right);
		vai.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					numeroProduzioni = Integer.parseInt(tx.getText().toString());
				}catch(Exception e){
					errore = true;
				}
				if(!errore){
					reset.setVisibility(View.INVISIBLE);
					leftfirstfollowtable.removeAllViews();
					add_left_right();
					}
				else{
					Toast.makeText(MainActivity.this, "SICURO SIA UN NUMERO INTERO?!", Toast.LENGTH_SHORT).show();
					errore = false;
				}
			}
		});
	}

	public void add_left_right(){
		try{
			Toast.makeText(this, "Aggiungo " + tx.getText().toString() + " Left/Right", Toast.LENGTH_SHORT).show();
			tableLayout.requestFocus();
			tableLayout.removeAllViews();
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			for(int pos= 0; pos<Integer.parseInt(tx.getText().toString()); pos++){
				View newView = inflater.inflate(R.layout.left_right, null);
				tableLayout.addView(newView, pos);
			}
			calcola = (Button) findViewById(R.id.btn_calcola);
			calcola.setVisibility(View.VISIBLE);
			calcola.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
//					calcola.setVisibility(View.INVISIBLE);
					calcola();
				}

			});
		}catch(Exception e){
			
		}
	}
	
	@SuppressLint("NewApi")
	public void calcola() {
		String left[] = new String[numeroProduzioni];
		String right[] = new String[numeroProduzioni];
		for (int i = 0; i < tableLayout.getChildCount(); i++) {
		    View view = tableLayout.getChildAt(i);
		
		    EditText rright = (EditText) view.findViewById(R.id.right);
		    EditText lleft = (EditText) view.findViewById(R.id.left);
		    
		    right[i] = rright.getText().toString().trim();
		    left[i] = lleft.getText().toString().trim();
		    
		    String[] controlloleftrecursive = right[i].split("/");
//		    if(controlloleftrecursive.length > 1){
//		    	controlloleftrecursive[1].charAt(0);
//		    }
		    for(int j = 0; j < controlloleftrecursive.length; j++){
		    		if(left[i].charAt(0) == controlloleftrecursive[j].charAt(0)){
		    			Toast.makeText(this, "ERRORE NELLA RIGA #" + (i+1) +"\nLEFT-RECURSIVE FOUND", Toast.LENGTH_SHORT).show();
				    	errore=true;
		    	}
		    }
		    if(left[i].length()!=1 || left[i].length()==0 || right[i].length()==0){
		    	Toast.makeText(this, "ERRORE NELLA RIGA #" + (i+1), Toast.LENGTH_SHORT).show();
		    	errore=true;
		    }
		}
		if(!errore){
			Log.d("conv", "procedo... non ci sono errori nell'input");
			leftfirstfollowtable.removeAllViews();
		  ArrayList<String> lleft = new ArrayList<String>();
		  ArrayList<String> ffirst = new ArrayList<String>();
		  ArrayList<String> ffollow = new ArrayList<String>();
		  
		  int numeroRighe = 0;
		  
		  for (int i = 0; i < numeroProduzioni; i++) {
			
			  numeroRighe++;
			  
			  if(right[i].contains("/"))
				  numeroRighe++;
				  
		}
		  
	      String right1[][]=new String[numeroRighe+1][6];
	      for(int i  = 0; i < right.length ;i++)
	        {
	          right1[i]=right[i].split("/");
	        }

	      for(int i  = 0; i < left.length ;i++)
	          {
	    	  		lleft.add(left[i]);
	           }
	      Log.d("conv", "end lleft");
	     String first[][]= new String[right.length][2];

	     for(int i  = 0; i < right.length ;i++)
	        {
	         int k = i;

	         for( int j  = 0; j < right1[k].length ;j++)
	            {
	              if(right1[k][j].charAt(0)< 'A' || right1[k][j].charAt(0) >'Z')
	                  {
	                   first[i][j]=""+right1[k][j].charAt(0);
	                  }
	              else
	                 {
	                  for(int h = 0; h < left.length ; h++ )
	                    {
	                     if(left[h].charAt(0)==(right1[k][j].charAt(0)))
	                       {
	                        k = h;
	                        j =-1;
	                        break;
	                      }
	                    }
	                  }
	              }
	           }
	     Log.d("conv", "init save ffirst");
	         System.out.print("\nFirst");
	         String comp = "";
	         for(int i  = 0; i < first.length ;i++)
	          {
	           System.out.print("\n");
	           comp="";
	           for(int j = 0; j < first[i].length ;j++)
	               {
	                System.out.print(first[i][j]+" ");
	                if(first[i][j] == null && !comp.contains("null ")){
//	                	Toast.makeText(MainActivity.this, "i=" + String.valueOf(i)+"::j="+ String.valueOf(j)+"::"+first[i][j], Toast.LENGTH_SHORT).show();
	                	comp=comp+first[i][j]+" ";
	                }else if(first[i][j] == null && comp.contains("null ")){
	                	//non scrivere altri null
	                }else{
	                	comp=comp+first[i][j]+" ";
	                }
	           }
	           String test = comp.replaceAll("null ", "");
	           if(test!=""){
	        	   comp=comp.replaceAll("null ", "");
	           }
	           ffirst.add(comp);
	          }
	         Log.d("conv", "end ffirst");
	         String follow[][]= new String[10][20];
	         int fcount[]= new int[10];
	         follow[0][0]="$";
	         fcount[0]=1;
	         for(int i = 0; i < left.length; i++){
	        	 if(i>0)
	               {
	                fcount[i]=0;
	               }
	             System.out.print("\n");

	             for( int j  = 0; j < right.length ;j++)
	                {
	                 for( int h  = 0; h < right1[j].length ;h++)
	                    {
	                     if(right1[j][h].contains(left[i]))
	                       {
	                         int B = right1[j][h].indexOf(left[i]);
	                         String a =right1[j][h].substring(0, B);
	                         String b =right1[j][h].substring(B+1,right1[j][h].length());

	                         if(b.isEmpty())
	                           {
	                            for(int k = 0; k < fcount[j] && j!= i ;k++)                                {                                                                  follow[i][fcount[i]++]= follow[j][k];                                }                            }                         else                           {                            if((int)b.charAt(0)>='A'&& (int)b.charAt(0)<='Z')
	                             {
	                              for(int k = 0; k < left.length ;k++)
	                                 {
	                                  if(left[k].equalsIgnoreCase(b))
	                                    {
	                                     for(int m = 0; m < first[k].length ;m++)
	                                        {
	                                         if(first[k][m].equalsIgnoreCase("e"))
	                                           {

	                                           }
	                                         else
	                                           {
	                                            follow[i][fcount[i]++]=first[k][m];
	                                           }
	                                        }
	                                      break;
	                                     }

	                                  }
	                              for(int k = 0; k < fcount[j] && j!= i;k++)
	                                 {
	                                   follow[i][fcount[i]++]= follow[j][k];
	                                 }

	                            }
	                         else
	                            {
	                              follow[i][fcount[i]++]=b;
	                            }
	                        }
	                     }
	                   }
	                }
	            }

	          System.out.print("\nFOLLOW");
	          for(int i  = 0; i < left.length ;i++)
	          {
	           System.out.print("\n");
	           for(int j = 0; j < fcount[i] ;j++)
	               {
	                System.out.print("  "+follow[i][j]);
	                ffollow.add(follow[i][j]);
	               }
	           }
	          Log.d("conv", "end ffollow");
	     try{
	  			
	  			leftfirstfollowtable.requestFocus();
	  			leftfirstfollowtable.removeAllViews();
	  			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	  			
	  			for(int pos= 0; pos<=numeroProduzioni; pos++){
	  				if(pos==0){
	  					View newView = inflater.inflate(R.layout.last_headers, null);
	  					leftfirstfollowtable.addView(newView, pos);
	  				}else{
		  				View newView = inflater.inflate(R.layout.last, null);
		  				TextView txtleft = (TextView) newView.findViewById(R.id.txtleft);
		  			    TextView txtfirst = (TextView) newView.findViewById(R.id.txtfirst);
		  			    TextView txtfollow = (TextView) newView.findViewById(R.id.txtfollow);
		  			    
		  			    try{txtleft.setText(lleft.get(pos-1).toString());}catch(Exception e){txtleft.setText("");}
		  			  try{txtfirst.setText(ffirst.get(pos-1).toString());}catch(Exception e){txtfirst.setText("");}
		  			try{txtfollow.setText(ffollow.get(pos-1).toString());}catch(Exception e){txtfollow.setText("");}
		  				leftfirstfollowtable.addView(newView, pos);
	  				}
	  			}
	  			reset = (Button) findViewById(R.id.btn_clear);
	  			reset.setVisibility(View.VISIBLE);
	  			reset.setOnClickListener(new View.OnClickListener() {
	  				@Override
	  				public void onClick(View v) {
	  					reset.setVisibility(View.INVISIBLE);
	  					tableLayout.removeAllViews();
	  					leftfirstfollowtable.removeAllViews();
	  				}
	  			});
	  		}catch(Exception e){
	  			
	  		}
			}else{
				Log.d("conv", "non procedo, ci sono degli errori nell'input");
				errore=false;
				calcola.setVisibility(View.VISIBLE);
			}
		}
	
	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

}
